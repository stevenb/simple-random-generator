import unittest
import randomgen as rand



class TestRandomGen(unittest.TestCase):

    def test_random_distributation(self):
        eps = 0.01
        reps = int(1e6)
        counter_next_num = {-1: 0, 0: 0, 1: 0, 2: 0, 3: 0}

        random_nums = [-1, 0, 1, 2, 3]
        probabilities = [0.01, 0.3, 0.58, 0.1, 0.01]
        gen = rand.RandomGen(random_nums, probabilities)

        for i in range(reps):
            rnd_num_next_num = gen.next_num()
            counter_next_num[rnd_num_next_num] += 1

        for rnd, rnd_occurrences in counter_next_num.items():
            next_num_pct = rnd_occurrences/reps
            index = random_nums.index(rnd)
            diff = abs(probabilities[index] - next_num_pct)

            assert diff < eps, "next_num() does not seem to weight the random numbers according to the given self._probabilities"


    # test corner cases
    def test_empty_population(self):
        probabilities = [0.01, 0.3, 0.58, 0.1, 0.01]
        with self.assertRaises(AssertionError):
            rand.RandomGen([], probabilities)

    def test_empty_probabilites(self):
        random_nums = [-1, 0, 1, 2, 3]
        with self.assertRaises(AssertionError):
            rand.RandomGen(random_nums, [])

    def test_equal_length(self):
        random_nums = [-1, 0, 1, 2, 3, 99]
        probabilities = [0.01, 0.3, 0.58, 0.1, 0.01]
        with self.assertRaises(AssertionError):
            rand.RandomGen(random_nums, probabilities)



if __name__ == "__main__":
    unittest.main()
