import random
import numpy as np



class RandomGen(object):
    # Values that may be returned by next_num()
    _random_nums = []
    # Probability of the occurrence of random_nums
    _probabilities = []
    _cumulative_weights = []

    def __init__(self, random_nums, probabilities):
        assert len(random_nums) > 0, "random_nums is empty"
        assert len(probabilities) > 0, "probabilities is empty"
        assert len(random_nums) == len(probabilities), "random_nums and probabilities must have the same length"

        self._random_nums = random_nums
        self._probabilities = probabilities
        self._cumulative_weights = list(np.multiply(np.cumsum(self._probabilities), 100).astype(int))

    def next_num(self):
        """
        Returns one of the randomNums. When this method is called
        multiple times over a long period, it should return the
        numbers roughly with the initialized probabilities.
        """
        last_cum_weight = self._cumulative_weights[-1]
        rnd = random.random()*last_cum_weight
        index = self.find_ceiling(rnd)

        return self._random_nums[index]

    def next_num_choices(self):
        """
        Returns:
            Same as next_num() but here we are using random.choices() to check whether
            next_num() behaves as expected.
        """
        return random.choices(self._random_nums, weights=self._probabilities)

    def find_ceiling(self, rnd_number):
        """
        Returns:
            This returns the index of the rnd_number within the population (self._random_nums)
        """
        low = 0
        high = len(self._random_nums)-1
        while low < high:
            mid = (high + low) // 2
            if rnd_number < self._cumulative_weights[mid]:
                high = mid
            else:
                low = mid + 1

        return low


if __name__ == "__main__":
    random_nums = [-1, 0, 1, 2, 3]
    probabilities = [0.01, 0.3, 0.58, 0.1, 0.01]
    gen = RandomGen(random_nums, probabilities)

    print("gen.next_num(): %s" % gen.next_num())
